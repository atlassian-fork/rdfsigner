rdfsigner - a script to generate an update manifesto for Firefox / Thunderbird add-ons.

rdfsigner is written in Python and is a replacement for Perl script uhura (mxtools). 

## Requirements

Python version: 2.7.6

Python modules: pyasn1, pycrypto

## Usage
```
python rdfsigner.py -k path-to-key.pem -o output.rdf extension.xpi "http://example.com/extension-update-url"
```
