# -*- coding: utf-8 -*-
# Copyright © 2014 - 2014 Atlassian Pty Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import base64
import hashlib
import sys

from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder as derEncoder
from xml.dom import minidom
from xml.dom.minidom import Document
from zipfile import ZipFile
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA512

# Parse command line
parser = argparse.ArgumentParser(description='rdf-signer is a Python script for signing update RDF manifests.')
parser.add_argument('-k', metavar='key.pem', help='The private key to sign with.')
parser.add_argument('-o', metavar='update.rdf', help='Output file')
parser.add_argument('-p', metavar='password', help='Key password')
parser.add_argument('xpi', metavar='extension.xpi', help='XPI file', nargs='?')
parser.add_argument('rdf', metavar='unsigned.rdf', help='Unsigned rdf file, to use instead of xpi file', nargs='?')
parser.add_argument('url', metavar='URL', help='http://www.example.com/extension.xpi', nargs='?')

args = parser.parse_args()
# No arguments are given, print help and exit
# if args.k == None and args.o == None and args.xpi == None and args.rdf == None:
#     parser.print_help()
#     sys.exit(0)


####################################################################################

# Extracts install.rdf file from xpi to memory
def extractInstallRdf(filepath):
    zipFile = ZipFile(filepath)
    for name in zipFile.namelist():
        if name == 'install.rdf':
            return zipFile.read(name)
    return None

# Fetches rdf file from xpi or from a command line if given
# Reads file to memory
def getRdfFile(args):
    # TODO: properly parse xpi or a rdf file as now they are not exclusive
    global rdfFile, f
    rdfFile = ""
    if args.xpi != None:
        rdfFile = extractInstallRdf(args.xpi)
    else:
        # XPI file is not defined, reading rdf file specified
        with open(args.rdf, 'r') as f:
            rdfFile = f.read()

    return rdfFile

# Calculates SHA1 of file
def sha1File(filepath):
    sha = hashlib.sha1()
    with open(filepath, 'rb') as f:
        while True:
            block = f.read(2 ** 10) # Magic number: 1024 = one-megabyte blocks.
            if not block: break
            sha.update(block)
        return sha.hexdigest()

def getFileData(filePath):
    f = open(filePath, 'rb')
    data = f.read()
    f.close()
    return data


class AlgorithmIdentifier(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('oid', univ.ObjectIdentifier()),
        namedtype.OptionalNamedType('opt', univ.Any())
    )

class ManifestSignature(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('alg', AlgorithmIdentifier()),
        namedtype.NamedType('sig', univ.BitString())
    )

# Decodes base 64 and adds newline char after every 64 chars
def decodeBase64Pretty(binary):
    line = base64.b64encode(binary)
    strArr = list(line)

    result = ""
    counter = 0
    for c in strArr:
        if (counter % 64 == 0) and (counter != len(strArr) - 1):
            result += "\n        "
        result += c
        counter += 1
    result += "\n"
    return result

pkcs1 = univ.ObjectIdentifier('1.2.840.113549.1.1')
sha512WithRSAEncryption = pkcs1 + univ.ObjectIdentifier((13,))
def generateSignature(data, keyData):

    key = RSA.importKey(keyData)
    signer = PKCS1_v1_5.new(key)

    digest = SHA512.new()
    digest.update(data)

    rsaSignature = signer.sign(digest)
    rsaSignedHex = rsaSignature.encode('hex')

    # Setting ASN1 object properties for serialization signature
    alg = AlgorithmIdentifier()
    alg.setComponentByName('oid', sha512WithRSAEncryption)

    formattedBinaryString = "'%s'H" % rsaSignedHex
    signatureRaw = ManifestSignature()
    signatureRaw.setComponentByName('alg', alg)
    signatureRaw.setComponentByName('sig', univ.BitString(formattedBinaryString))

    binaryAsn1 = derEncoder.encode(signatureRaw)

    base64encoded = decodeBase64Pretty(binaryAsn1)
    return base64encoded

def createNodeWithTextValue(doc, nodeName, nodeValue):
    node = doc.createElement(nodeName)
    textNode = doc.createTextNode(nodeValue)
    node.appendChild(textNode)
    return node

def getValueFromDom(dom, tagName):
    parsedMinVersion = dom.getElementsByTagName(tagName)
    value = parsedMinVersion[0].firstChild.data
    return str(value)

def getValueFromTagretApplicaitonNode(dom, tagName):
    return getValueFromDom(dom, tagName)

def parseAppSubject(xmlString):
    # Parses em:id in em:targetApplication, but not from em:targetApplication (to ensure it, the substring is cut)

    emStartIndex = xmlString.find("<em:targetApplication>")
    emEndIndex = xmlString.find("</em:targetApplication>")

    # cut <em:targetApplication> with all it's content and closing tag
    xmlStringCut = xmlString[0:emStartIndex] + xmlString[emEndIndex + len("</em:targetApplication>"):]

    idStartIndex = xmlStringCut.find("<em:id>")
    idEndIndex = xmlStringCut.find("</em:id>", idStartIndex)
    suffixLen = len("<em:id>")
    parsedId = xmlString[idStartIndex + suffixLen:idEndIndex]
    return parsedId


def parseAppId(xmlString):
    '''
    Parses em:id in em:targetApplication

    <em:targetApplication>
        <Description>
        <em:id>{eb0b5de6-e936-4772-ac27-15eb191e3d84}</em:id>
        <em:minVersion>4.0</em:minVersion>
        <em:maxVersion>*</em:maxVersion>
        </Description>
    </em:targetApplication>
    '''

    emStartIndex = xmlString.find("<em:targetApplication>")
    idStartIndex = xmlString.find("<em:id>", emStartIndex)
    idEndIndex = xmlString.find("</em:id>", idStartIndex)
    suffixLen = len("<em:id>")
    parsedSubject = xmlString[idStartIndex + suffixLen:idEndIndex]

    return parsedSubject

def parseManifest(xmlString):
    '''
    Parses xml of the following structure

    <RDF xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:em="http://www.mozilla.org/2004/em-rdf#">

        <Description about="urn:mozilla:install-manifest">
            <em:id>addon-id</em:id>
            <em:version>@@build.version@@</em:version>
            <em:type>2</em:type>
            <em:bootstrap>true</em:bootstrap>

            <!-- Target Application this extension can install into with minimum and maximum supported versions. -->
            <em:targetApplication>
                <Description>
                    <em:id>{eb0b5de6-e936-4772-ac27-15eb191e3d84}</em:id>
                    <em:minVersion>4.0</em:minVersion>
                    <em:maxVersion>*</em:maxVersion>
                </Description>
            </em:targetApplication>


        </Description>
    </RDF>
    '''

    dom = minidom.parseString(xmlString)

    # TODO: get node by attribute value "urn:mozilla:install-manifest"
    # TODO: look for all pieces of information since that node


    # parses from targetApplication section
    xpathNamespaces = {'em': 'http://www.mozilla.org/2004/em-rdf#'}

    # parses from targetApplication section
    # TODO: move to xpath parsing
    parsedMinVersion = getValueFromTagretApplicaitonNode(dom, "em:minVersion")
    parsedMaxVersion = getValueFromTagretApplicaitonNode(dom, "em:maxVersion")
    parsedVersion = getValueFromDom(dom, "em:version")

    # Parses from root description ID
    parsedId = parseAppId(xmlString)
    parsedSubject = parseAppSubject(xmlString)

    return {
        'id': parsedId,
        'subject': parsedSubject,
        'minVersion' : parsedMinVersion,
        'maxVersion' : parsedMaxVersion,
        'version' : parsedVersion
    }

def fixPadding(stringXml):
    result = ""

    counter = 0
    xmlLines = stringXml.splitlines()
    for line in xmlLines:
        # just the line before the last closing tag
        if counter == len(xmlLines) - 2:
            # Hackery for prettyxml - adding missing whitespace (as it is in Python 2.7.6)
            result += "    %s\n" % (line)
        else:
            # Just original line
            result += "%s\n" % (line)
        counter += 1


    return result

def getXmlUnsigned(updateData):
    doc = Document()
    root = doc.createElement("RDF:Description")
    root.setAttribute("about", updateData['subject'])

    doc.appendChild(root)

    updates = doc.createElement("em:updates")
    root.appendChild(updates)

    seq = doc.createElement("RDF:Seq")
    updates.appendChild(seq)

    li = doc.createElement("RDF:li")
    seq.appendChild(li)

    listDescription = doc.createElement("RDF:Description")
    li.appendChild(listDescription)

    emTarget = doc.createElement("em:targetApplication")
    listDescription.appendChild(emTarget)

    targetDescription = doc.createElement("RDF:Description")
    emTarget.appendChild(targetDescription)

    targetDescription.appendChild(createNodeWithTextValue(doc, "em:id", updateData['id']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:maxVersion", updateData['maxVersion']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:minVersion", updateData['minVersion']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:updateHash", updateData['updateHash']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:updateLink", updateData['updateLink']))

    listDescription.appendChild(createNodeWithTextValue(doc, "em:version", updateData['version']))

    fullXml = doc.toprettyxml(indent="  ", newl="\n", encoding="UTF-8")

    # substring XML header "<?xml version="1.0" encoding="UTF-8"?>" here:
    newLineIndex = fullXml.find('\n')
    processedXml = fullXml[newLineIndex + 1:]
    return processedXml

def getXmlSigned(updateData):
    doc = Document()
    root = doc.createElement("rdf:Description")

    root.setAttribute("rdf:about", updateData['subject'])
    doc.appendChild(root)

    updates = doc.createElement("em:updates")
    root.appendChild(updates)

    seq = doc.createElement("rdf:Seq")
    updates.appendChild(seq)

    li = doc.createElement("rdf:li")
    seq.appendChild(li)

    listDescription = doc.createElement("rdf:Description")
    li.appendChild(listDescription)
    listDescription.appendChild(createNodeWithTextValue(doc, "em:version", updateData['version']))

    emTarget = doc.createElement("em:targetApplication")
    listDescription.appendChild(emTarget)

    targetDescription = doc.createElement("rdf:Description")
    emTarget.appendChild(targetDescription)

    targetDescription.appendChild(createNodeWithTextValue(doc, "em:id", updateData['id']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:minVersion", updateData['minVersion']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:maxVersion", updateData['maxVersion']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:updateLink", updateData['updateLink']))
    targetDescription.appendChild(createNodeWithTextValue(doc, "em:updateHash", updateData['updateHash']))

    root.appendChild(createNodeWithTextValue(doc, "em:signature", updateData['signature']))

    # inner xml has <rdf:Description>
    innerXml = doc.toprettyxml(indent="    ", newl="\n", encoding="UTF-8")

    # substring XML header "<?xml version="1.0" encoding="UTF-8"?>" here:
    newLineIndex = innerXml.find('\n')
    processedXml = innerXml[newLineIndex:]


    # wrapping inner xml with <rdf:RDF> tag
    headerXml = '<?xml version="1.0" encoding="UTF-8"?>\n' \
                '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"\n' \
                '        xmlns:em="http://www.mozilla.org/2004/em-rdf#">\n'
    footerXml = '</rdf:RDF>'

    # Python doesn't add padding in element
    fixedPaddingXml = fixPadding(processedXml)

    tabbedXml = ""
    for line in fixedPaddingXml.splitlines():
        if line == "":
            continue
        tabbedXml += "    %s\n" % (line)
    # concat header xml, tabbed xml and footer xml
    finalXml = '%s%s%s\n' % (headerXml, tabbedXml, footerXml)


    return finalXml

def writeDataToFile(filePath, dataRaw):
    file = open(filePath, "wb")
    file.write(dataRaw)
    file.close()

def writeUpdateRdf(xpiFilePath, rsaPemKeyPath, updateLink, outFilePath):
    # Load all content of files in memory
    installRdf = extractInstallRdf(xpiFilePath)
    updateHash = sha1File(xpiFilePath)
    rsaPemKeyData = getFileData(rsaPemKeyPath)
    updateRdfData = generateUpdateRdf(installRdf, rsaPemKeyData, updateHash, updateLink)

    writeDataToFile(outFilePath, updateRdfData)

def generateUpdateRdf(rdfFileData, rsaPemKeyData, updateHash, updateLink):
    manifest = parseManifest(rdfFileData)

    updateUrl = str(updateLink)

    parametersUnsigned = getXmlUnsigned({
        'id' : manifest['id'],
        'subject' : 'urn:mozilla:extension:' + manifest['subject'],
        'maxVersion' : manifest['maxVersion'],
        'minVersion' : manifest['minVersion'],
        'updateHash' : 'sha1:' + updateHash,
        'updateLink' : updateUrl,
        'version' : manifest['version']
    })

    signature = generateSignature(parametersUnsigned, rsaPemKeyData)

    parametersSigned = {
        'id' : manifest['id'],
        'subject' : 'urn:mozilla:extension:' + manifest['subject'],
        'maxVersion' : manifest['maxVersion'],
        'minVersion' : manifest['minVersion'],
        'updateHash' : 'sha1:' + updateHash,
        'updateLink' : updateUrl,
        'version' : manifest['version'],
        'signature' : signature,
        }
    xmlSigned = getXmlSigned(parametersSigned)
    return xmlSigned

####################################################################################

if (args.xpi):
    writeUpdateRdf(args.xpi, args.k, args.url, args.o)




